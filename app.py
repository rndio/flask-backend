from flask import Flask, request, jsonify
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from config import Config
from db import init_db
from models import User
import requests

app = Flask(__name__)
app.config.from_object(Config)

API_KEY = "6549071c1ef58bf4a789d8cb30827f55"
API_URL = "https://api.rajaongkir.com/starter"
headers = {'key': API_KEY}

# Init Db
init_db(app)
jwt = JWTManager(app)

@app.route("/")
def hello_world():
    return "Rendio Simamora - Flask API Rajaongkir"

@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    username = data.get('username')
    password = data.get('password')

    user = User.find_by_username(username)
    if user and User.check_password(user[3], password):
        access_token = create_access_token(identity={'username': username})
        return jsonify(access_token=access_token), 200
    
    return jsonify({"msg": "Bad username or password"}), 401

@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    name = data.get('name')
    username = data.get('username')
    password = data.get('password')
    
    if User.find_by_username(username):
        return jsonify({"msg": "User already exists"}), 409

    User.create_user(name, username, password)
    return jsonify({"msg": "User created successfully"}), 201

@app.route('/protected', methods=['GET'])
@jwt_required()
def protected():
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200

@app.route('/users', methods=['GET'])
@jwt_required()
def users():
    users = User.get_all_users()
    return jsonify(users), 200

# Route untuk mendapatkan data provinsi
@app.route("/rajaongkir/province/", methods=['GET'])
def province_get():
    apiURL = API_URL + "/province"
    params = {}

    # Jika terdapat query parameter id
    if request.args.get('id'):
      idProvinsi = request.args.get('id')
      params['id'] = idProvinsi

    # Gabungkan query parameter di apiURL
    if params:
      apiURL = f"{apiURL}?{'&'.join([f'{key}={value}' for key,value in params.items()])}"

    response = requests.get(f"{apiURL}", headers=headers)
    return response.json().get('rajaongkir').get('results')

# Route untuk mendapatkan data kota
@app.route("/rajaongkir/city/", methods=['GET'])
def cities_get():
    apiURL = API_URL + "/city"
    params = {}

    # Jika terdapat query parameter id
    if request.args.get('id'):
      idKota = request.args.get('id')
      params['id'] = idKota

    # Jika terdapat query parameter province
    if request.args.get('province'):
      idProvinsi = request.args.get('province')
      params['province'] = idProvinsi

    # Gabungkan query parameter di apiURL
    if params:
      apiURL = f"{apiURL}?{'&'.join([f'{key}={value}' for key,value in params.items()])}"

    response = requests.get(f"{apiURL}", headers=headers)
    return response.json().get('rajaongkir').get('results')

# Route untuk mendapatkan data biaya pengiriman
@app.route("/rajaongkir/cost/", methods=['POST'])
def cost_get():
    apiURL = API_URL + "/cost"
    params = {}

    # Jika terdapat query parameter origin
    if request.form.get('origin'):
      origin = request.form.get('origin')
      params['origin'] = origin

    # Jika terdapat query parameter destination
    if request.form.get('destination'):
      destination = request.form.get('destination')
      params['destination'] = destination

    # Jika terdapat query parameter weight
    if request.form.get('weight'):
      weight = request.form.get('weight')
      params['weight'] = weight

    # Jika terdapat query parameter courier
    if request.form.get('courier'):
      courier = request.form.get('courier')
      params['courier'] = courier

    response = requests.post(f"{apiURL}", headers=headers, data=params)
    return response.json().get('rajaongkir')


if __name__ == "__main__":
    app.run(port=5000, debug=True)