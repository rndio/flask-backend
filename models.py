from werkzeug.security import generate_password_hash, check_password_hash
from db import mysql

class User:
    @staticmethod
    def create_user(name, username, password):
        cursor = mysql.connection.cursor()
        password_hash = generate_password_hash(password)
        cursor.execute("INSERT INTO users (name, username, password) VALUES (%s, %s, %s)", (name, username, password_hash))
        mysql.connection.commit()
        cursor.close()

    @staticmethod
    def find_by_username(username):
        cursor = mysql.connection.cursor()
        cursor.execute("SELECT * FROM users WHERE username = %s", (username,))
        user = cursor.fetchone()
        cursor.close()
        return user

    @staticmethod
    def check_password(stored_password, provided_password):
        return check_password_hash(stored_password, provided_password)

    @staticmethod
    def get_all_users():
        cursor = mysql.connection.cursor()
        cursor.execute("SELECT id,name,username FROM users")
        users = cursor.fetchall()
        cursor.close()
        return users
